#!/usr/bin/env sh

if [ "$FEDI_USER" = "" ]; then
  echo 'Please specify $FEDI_USER as an environment variable.'
  exit 1
fi

if [ "$FEDI_INST" = "" ]; then
  echo 'Please specify $FEDI_INST as an environment variable.'
  exit 2
fi

KONG=$(cat adjectives/2syllableadjectives.txt nouns/2syllablenouns.txt \
  | sed 's/\r$//' \
  | grep '\(y\|ie\|ee\|i\)$' \
  | shuf -n 1 \
  | sed 's/^./\U&/' \
  | head -c-1 \
  | xargs -0 printf '%s Kong\n' \
)

PATH="$PATH:$HOME/.bin"
madonctl --login "$FEDI_USER" --instance "$FEDI_INST" post "$KONG"
